#!/usr/bin/env bash

show_help() {
  echo "-u|--url <value>      HTTP git location of the project to build"
  echo "-c|--commit <value>   Commit hash of the project to build"
}

url=""
commit=""

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -u|--url)
        url=$2
        shift
        ;;
    -c|--commit)
        commit=$2
        shift
        ;;
    -h|--help)
        show_help
        exit 1
        ;;
    *)
        echo "Unkown option: $key"
        show_help
        exit 1
        ;;
esac
shift # past argument or value
done



echo "Url=[${url}]"
echo "Commit=[${commit}]"

#Check for required arguments
arguments_failed=0
if [ "${url}" == "" ]; then
  echo "Url is a required parameter (-u|--url <value>)"
  arguments_failed=1
fi
if [ "${commit}" == "" ]; then
  echo "Commit is a required parameter (-c|--commit <value>)"
  arguments_failed=1
fi

#Exit if needed
if [ "${arguments_failed}" != "0" ]; then
  exit 1
fi

git clone -n "${url}" project
cd project
git checkout "${commit}"

git submodule init
git submodule update --recursive

mvn package
exit=${?}

if [ "${exit}" == "0" ]; then
  release=$(find . -name "*.tar.gz")
  printf "${GREEN}Release: %s\n${NORMAL}" "${release}"
  exit 0
else
  printf "Package failed"
  exit 1
fi

#printf "%-30s${NORMAL}" "Updating submodules."
#git submodule init >/tmp/build.log 2>&1
#git submodule update --recursive >/tmp/build.log 2>&1
#BLACK=$(tput setaf 0)
#RED=$(tput setaf 1)
#GREEN=$(tput setaf 2)
#YELLOW=$(tput setaf 3)
#LIME_YELLOW=$(tput setaf 190)
#POWDER_BLUE=$(tput setaf 153)
#BLUE=$(tput setaf 4)
#MAGENTA=$(tput setaf 5)
#CYAN=$(tput setaf 6)
#WHITE=$(tput setaf 7)
#BRIGHT=$(tput bold)
#NORMAL=$(tput sgr0)
#BLINK=$(tput blink)
#REVERSE=$(tput smso)
#UNDERLINE=$(tput smul)
#
#printf "Building project %s on commit %s\n${NORMAL}" "${url}" "${commit}"
#
#timedCommand "" "Cloning project."
#t1=$(date +%s)
#printf "%-30s${NORMAL}" "Cloning project."
#git clone -n "${url}" project >/tmp/build.log 2>&1
#cd project >/tmp/build.log 2>&1
#git checkout "${commit}" >/tmp/build.log 2>&1
#t2=$(date +%s)
#printf "Finished in %s seconds\n${NORMAL}" "$(expr $t2 - $t1)"
#
#printf "%-30s${NORMAL}" "Updating submodules."
#git submodule init >/tmp/build.log 2>&1
#git submodule update --recursive >/tmp/build.log 2>&1
#
#t3=$(date +%s%N)
#printf "Finished in %s seconds\n${NORMAL}" "$(expr $t3 - $t2)"
#
#printf "%-30s${NORMAL}" "Testing."
#mvn test >/tmp/build.log 2>&1
#
#t4=$(date +%s%N)
#printf "${GREEN}Finished in %s seconds\n${NORMAL}" "$(expr $t4 - $t3)"
#
#
#printf "${GREEN}%-30s${NORMAL}" "Compiling and packaging."
#mvn package -Dmaven.test.skip=true >/tmp/build.log 2>&1
#
#t5=$(date +%s%N)
#printf "${GREEN}Finished in %s seconds\n${NORMAL}" "$(expr $t5 - $t4)"
#
##Find release name
#release=$(find . -name "*.tar.gz")
#printf "${GREEN}Release: %s\n${NORMAL}" "${release}"
#
#timedCommand() {
#  t1=$(date +%s)
#  printf "%-30s${NORMAL}" "Cloning project."
#  git clone -n "${url}" project >/tmp/build.log 2>&1
#  cd project >/tmp/build.log 2>&1
#  git checkout "${commit}" >/tmp/build.log 2>&1
#  t2=$(date +%s)
#  printf "Finished in %s seconds\n${NORMAL}" "$(expr $t2 - $t1)"
#}